const moment = require("moment");

const formatDateNow = () => {
  return moment().format("YYYY/MM/DD hh:mm:ss");
};

const formatDatePost = (date) => {
  return moment(new Date(date)).format("YYYY/MM/DD kk:mm");
};

const formatDatePostShort = (date) => {
  return moment(new Date(date)).format("YYYY/MM/DD");
};

const formatDateShort = (date) => {
  return moment(new Date(date)).format("DD/MM/YYYY");
};

module.exports = {
  formatDatePost,
  formatDatePostShort,
  formatDateShort,
};