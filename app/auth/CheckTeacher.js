const AccountModel = require("../models/Account");

module.exports = async (req, res, next) => {
  try {
    const user = await AccountModel.findOne({ _id: req.user._id });
    if (user.role === "teacher") {
      next();
    } else {
      res.status(402).json({
        status: 402,
        success: false,
        message: "Login with wrong authorization",
      });
    }
  } catch (error) {
    res.status(402).send({
      status: 402,
      success: false,
      message: "Login with wrong authorization",
    });
  }
};
