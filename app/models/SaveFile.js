const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const SaveFile = new Schema(
  {
    idAccount: { type: Number },
    fullName: {
      type: String,
    },
    idCourse: {
      type: String,
    },
    nameCourse: { type: String },
    role: { type: String }, // 2 phan quyen gv va hs
    idExercise: { type: String },
    linkFile: {
      type: Array,
      default: [],
    },
  },
  {
    collection: "SaveFile",
  }
);
module.exports = mongoose.model("SaveFile", SaveFile);
