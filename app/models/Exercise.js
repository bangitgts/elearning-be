const mongoose = require("mongoose");
// const formatDate = require("../utils/FormatDate");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const Exercise = new Schema(
  {
    _id: {
      type: Number,
    },
    idTeacher: { type: Number },
    idCourse: {
      type: String,
    },
    nameCourse: {
      type: String,
    },
    titleExercise: {
      type: String,
      default: null,
    },
    isTextPoint: {
      type: Number,
      default: 0,
    },
    fileUpload: {
      type: Array,
      default: [],
    },
    descriptionExercise: {
      type: String,
      default: null,
    },
    allowSubmission: {
      type: Date,
    },
    submissionDeadline: {
      type: Date,
      default: null,
    },
    createDate: {
      type: Date,
      default: Date.now(),
    },
    updateDate: {
      type: Date,
      default: null,
    },
  },
  {
    _id: false,
    collection: "Exercise",
  }
);
Exercise.plugin(AutoIncrement, { id: "Exercise", inc_field: "_id" });
Exercise.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("Exercise", Exercise);
