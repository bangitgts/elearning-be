const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const ClassSchema = new Schema(
  {
    _id: {
      type: Number,
    },
    nameClass: {
      type: String,
    },
    lv: { type: Number },
  },
  {
    _id: false,
    collection: "Class",
  }
);

ClassSchema.plugin(AutoIncrement, {id: 'Class', inc_field: '_id' });
ClassSchema.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("Class", ClassSchema);
