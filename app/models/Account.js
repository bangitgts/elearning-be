const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const AccountSchema = new Schema(
  {
    _id: {
      type: Number,
    },
    fullName: {
      type: String,
      minLength: 1,
      required: [true, "fullName required"],
    },
    idClass: {
      type: Number,
      default: null,
    },
    nameClass: {
      type: String,
      default: null,
    },
    address: {
      type: String,
      default: null,
    },
    dateBirth: {
      type: Date,
      default: null,
    },
    phoneNumber: {
      type: String,
      default: null,
    },
    parentName: {
      type: String,
      default: null,
    },
    phoneNumberParent: {
      type: String,
      default: null,
    },
    role: {
      type: String,
      default: "student",
    },
    username: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, "User required"],
    },
    email: {
      type: String,
      default: "",
    },
    password: {
      type: String,
      required: [true, "Password required"],
    },
    tokenResetPassword: {
      type: String,
      default: null,
    },
    gender: {
      type: Number, // 0 la nu, 1 la nam
      require: 0,
    },
    avatar: {
      type: String,
      default: "abc",
    },
    createDate: {
      type: Date,
    },
  },
  {
    _id: false,
    collection: "Account",
  }
);

// Add plugin
AccountSchema.plugin(AutoIncrement, { id: "Account", inc_field: "_id" });
AccountSchema.plugin(mongooseDelete, { deleteAt: true, overrideMethods: "all" });
module.exports = mongoose.model("Account", AccountSchema);
