const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const Course = new Schema(
  {
    _id: { 
      type: String
    },
    nameCourse: {
      type: String,
    },
    fullNameTeacher: {
      type: String,
    },
    idTeacher: {
      type: Number,
    },
    studentList: {
      type: Array,
      default: [],
    },
    createDate: {
      type: Date,
      default: Date.now(),
    },
  },
  {
    _id: false,
    collection: "Course",
  }
);
Course.plugin(mongooseDelete, {deleteAt: true,overrideMethods: "all",});
module.exports = mongoose.model("Course", Course);
