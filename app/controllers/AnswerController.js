const AnswerModel = require("../models/Answer");
const ExerciseModel = require("../models/Exercise");
const { flagTime } = require("../utils/FlagTime");
const Pagination = require("../utils/Pagination");

class AnswerController {
  async addAnswer(req, res) {
    const idAccount = req.user._id;
    const idExercise = parseInt(req.body.idExercise);
    const descriptionAnswer = req.body.descriptionAnswer;
    try {
      const checkExercise = await ExerciseModel.findOne({ _id: idExercise });
      const checkAnswer = await AnswerModel.findOne({ idAccount: idAccount, idExercise: idExercise });
      const checkTimeSubission = flagTime(checkExercise.allowSubmission, checkExercise.submissionDeadline);

      if (!checkTimeSubission) {
        return res.status(403).json({
          message: "Exam time is over",
          success: false,
          status: 403,
        });
      }

      if (!checkAnswer && checkTimeSubission) {
        if (descriptionAnswer || req.files.length > 0) {
          AnswerModel.create({
            idExercise: idExercise,
            idAccount: idAccount,
            idCourse: checkExercise.idCourse,
            isTextPoint: checkExercise.isTextPoint,
            descriptionAnswer: descriptionAnswer,
            fileUpload: req.files,
          });
          return res.status(200).json({
            message: "Add answer successfully",
            success: true,
            status: 200,
          });
        } else {
          return res.status(402).json({
            message: "You have not entered anything, please fill in before submitting",
            success: false,
            status: 402,
          });
        }
      } else {
        return res.status(400).json({
          message: "Got the answer in Course",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        success: false,
        status: 400,
        message: error,
      });
    }
  }

  async editAnswer(req, res) {
    const idAnswer = req.body.idAnswer;
    const descriptionAnswer = req.body.descriptionAnswer;
    const fileKeep = req.body.fileKeep;
    const idExercise = parseInt(req.body.idExercise);

    try {
      const checkAnswer = await AnswerModel.findOne({ _id: idAnswer });
      const checkExercise = await ExerciseModel.findOne({ _id: idExercise });
      const checkTimeSubission = flagTime(checkExercise.allowSubmission, checkExercise.submissionDeadline);

      if (!checkTimeSubission) {
        return res.status(403).json({
          message: "Exam time is over",
          success: false,
          status: 403,
        });
      }
      let dataFile = [];
      console.log(fileKeep);
      if (fileKeep) {
        for (let item of checkAnswer.fileUpload) {
          const findItem = fileKeep.find((e) => e === item.filename);
          console.log(findItem);
          if (findItem) {
            dataFile.push(item);
          }
        }
      } else {
        dataFile = [...checkAnswer.fileUpload];
      }

      if (req.files?.length > 0) {
        dataFile = [...dataFile, ...req.files];
      }

      console.log(dataFile);
      if (checkAnswer && checkTimeSubission) {
        checkAnswer.descriptionAnswer = descriptionAnswer || checkAnswer.descriptionAnswer;
        checkAnswer.fileUpload = dataFile || checkAnswer.fileUpload;
        checkAnswer.save();
        return res.status(200).json({
          message: "Edit this answer in successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(400).json({
          message: "The answer not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        success: false,
        status: 500,
        message: error,
      });
    }
  }

  async changeAnswer(req, res, next) {
    const _idAnswer = req.query.idAnswer;
    const checkAnswer = AnswerModel.findOne({ _id: _idAnswer });
    try {
      if (checkAnswer && flagTime(allowSubmission, submissionDeadline)) {
        checkAnswer.descriptionAnswer = descriptionAnswer;
        checkAnswer.updateDate = Date.now();
        return res.status(200).json({
          message: "Add answer successfully",
          success: true,
          status: 200,
        });
      }
    } catch (error) {
      return res.status(500).json({
        success: false,
        status: 400,
        message: error,
      });
    }
  }

  async deleteAnswer(req, res) {
    const idAnswer = req.params.idAnswer;
    try {
      const findAnswer = await AnswerModel.findOne({ _id: idAnswer, idAccount: req.user._id });
      if (findAnswer) {
        AnswerModel.find({ _id: idAnswer })
          .remove()
          .exec()
          .then((data) => {
            return res.status(200).json({
              message: "Delete answer successfully",
              success: true,
              status: 200,
            });
          });
      } else {
        return res.status(404).json({
          message: "Answer not found",
          success: false,
          status: 404,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async getAnswerFromExercise(req, res) {
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const idExercise = req.query.idExercise;
    try {
      const data = await AnswerModel.find({ idExercise: idExercise });
      res.status(200).json({
        success: true,
        status_code: 200,
        setLoading: true,
        meta: {
          total: data.length,
          page_size: page_size,
          page_number: page_number,
        },
        data: Pagination(data, page_size, page_number),
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        setLoading: true,
        status_code: 500,
        message: error,
      });
    }
  } // get by idExercise

  async getInformationAnswer(req, res) {
    const idAccount = req.query.idAccount;
    const idAnswer = req.query.idAnswer;
    const findAnswer = await AnswerModel.findOne({ _id: idAnswer, idAccount: idAccount }).select("-__v");
    try {
      let data = [];
      let feedbackFromTeacherByImage = [];
      if (findAnswer.fileUpload) {
        for (let item of findAnswer.fileUpload) {
          const object = {
            fieldname: item.fieldname,
            originalname: item.originalname,
            filename: item.filename,
            pathname: `http://api.itcode.vn:3000/${item.path.slice(19)}`,
            mimetype: item.mimetype,
            //pathname: item.pathname,
            size: item.size,
          };
          data.push(object);
        }
      }
      if (findAnswer.feedbackFromTeacherByImage) {
        for (let item of findAnswer.feedbackFromTeacherByImage) {
          const object = {
            fieldname: item.fieldname,
            originalname: item.originalname,
            filename: item.filename,
            pathname: `http://api.itcode.vn:3000/${item.path.slice(19)}`,
            mimetype: item.mimetype,
            //pathname: item.pathname,
            size: item.size,
          };
          feedbackFromTeacherByImage.push(object);
        }
      }
      const item = {
        _id: findAnswer._id,
        idAccount: findAnswer.idAccount,
        idExercise: findAnswer.idExercise,
        descriptionAnswer: findAnswer.descriptionAnswer,
        fileUpload: data,
        studyPoint: findAnswer.studyPoint,
        isTextPoint: findAnswer.isTextPoint,
        feedbackFromTeacher: findAnswer.feedbackFromTeacher,
        feedbackFromTeacherByImage: feedbackFromTeacherByImage,
        createDate: findAnswer.createDate,
        updateDate: findAnswer.updateDate,
      };
      res.status(200).json({
        success: true,
        status_code: 200,
        data: item,
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
      });
    }
  }

  async gradingAssignment(req, res) {
    const idAnswer = req.body.idAnswer;
    const studyPoint = req.body.studyPoint;
    const feedbackFromTeacher = req.body.feedbackFromTeacher;
    try {
      const checkAnswer = await AnswerModel.findOne({ _id: idAnswer });
      if (checkAnswer) {
        //const checkExercise = await ExerciseModel.findOne({ _id: checkAnswer.idExercise });
        if (checkAnswer.isTextPoint === 0) {
          checkAnswer.studyPoint = studyPoint;
          checkAnswer.feedbackFromTeacher = feedbackFromTeacher;
          checkAnswer.feedbackFromTeacherByImage = req.files;
          checkAnswer.save();
          return res.status(200).json({
            message: "Grading assignment successfully",
            success: true,
            setLoading: true,
            status: 200,
          });
        } else {
          if (studyPoint == 0 || studyPoint == 1) {
            checkAnswer.studyPoint = parseInt(studyPoint);
            checkAnswer.feedbackFromTeacher = feedbackFromTeacher;
            checkAnswer.feedbackFromTeacherByImage = req.files;
            checkAnswer.save();
            return res.status(200).json({
              message: "Grading assignment successfully",
              success: true,
              status: 200,
            });
          } else {
            return res.status(400).json({
              message: "Input 0 or 1",
              success: false,
              status: 400,
            });
          }
        }
      } else {
        return res.status(400).json({
          message: "Answer not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Grading assignment failed",
        success: false,
        setLoading: true,
        status: 200,
      });
    }
  } // sử dụng luôn router này để cập nhật điểm nếu cần

  async editGradingAssignment(req, res) {
    const idAnswer = req.body.idAnswer;
    const studyPoint = req.body.studyPoint;
    const feedbackFromTeacher = req.body.feedbackFromTeacher;
    const fileKeep = req.body.fileKeep;
    const checkAnswer = await AnswerModel.findOne({ _id: idAnswer });

    let dataFile = [];
    if (fileKeep) {
      for (let item of checkAnswer.feedbackFromTeacherByImage) {
        const findItem = fileKeep.find((e) => e === item.filename);
        if (findItem) {
          dataFile.push(item);
        }
      }
    } else {
      dataFile = [...checkAnswer.feedbackFromTeacherByImage];
    }

    if (req.files?.length > 0) {
      dataFile = [...dataFile, ...req.files];
    }
    try {
      if (checkAnswer) {
        //const checkExercise = await ExerciseModel.findOne({ _id: checkAnswer.idExercise });
        if (checkAnswer.isTextPoint === 0) {
          checkAnswer.studyPoint = studyPoint || checkAnswer.studyPoint;
          checkAnswer.feedbackFromTeacher = feedbackFromTeacher || checkAnswer.feedbackFromTeacher;
          checkAnswer.feedbackFromTeacherByImage = dataFile || checkAnswer.feedbackFromTeacherByImage;
          checkAnswer.save();
          return res.status(200).json({
            message: "Change grading assignment successfully",
            success: true,
            setLoading: true,
            status: 200,
          });
        } else {
          if (studyPoint == 0 || studyPoint == 1) {
            checkAnswer.studyPoint = studyPoint || parseInt(checkAnswer.studyPoint);
            checkAnswer.feedbackFromTeacher = feedbackFromTeacher || checkAnswer.feedbackFromTeacher;
            checkAnswer.feedbackFromTeacherByImage = dataFile || checkAnswer.feedbackFromTeacherByImage;
            checkAnswer.save();
            return res.status(200).json({
              message: "Change grading assignment successfully",
              success: true,
              status: 200,
            });
          } else {
            return res.status(400).json({
              message: "Input 0 or 1",
              success: false,
              status: 400,
            });
          }
        }
      } else {
        return res.status(400).json({
          message: "Answer not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "System error",
        success: false,
        status: 500,
      });
    }
  }
}
module.exports = new AnswerController();
