const LectureModel = require("../models/Lecture");
const CourseModel = require("../models/Course");
const _ = require("lodash");
const { formatDatePost } = require("../utils/ParseTimeDay");

class LectureController {
  async openLecture(req, res) {
    const nameLecture = req.body.nameLecture;
    const descriptionLecture = req.body.descriptionLecture;
    const idCourse = req.body.idCourse;
    try {
      const checkIdCourse = await CourseModel.findOne({ _id: idCourse });
      if (checkIdCourse) {
        if (nameLecture) {
          LectureModel.create({
            nameLecture: nameLecture,
            descriptionLecture: descriptionLecture,
            idCourse: idCourse,
            fileUpload: req.files,
            createDate: Date.now(),
          });
          return res.status(200).json({
            message: "Add lecture successfully",
            success: true,
            status: 200,
          });
        } else {
          return res.status(400).json({
            message: "Must enter  the lecture name",
            success: false,
            status: 400,
          });
        }
      } else {
        return res.status(400).json({
          message: "Course Invalid",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async deleteLecture(req, res) {
    const idLecture = req.params.idLecture;
    try {
      const findLecture = await LectureModel.findOne({ _id: idLecture });
      if (findLecture) {
        LectureModel.remove({ _id: idLecture }, function (err) {
          if (!err) {
            return res.status(200).json({
              message: "Delete lecture successfully",
              success: true,
              status: 200,
            });
          } else {
            return;
          }
        });
      } else {
        return res.status(400).json({
          message: "Lecture not found",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async editLecture(req, res) {
    const idLecture = req.body.idLecture;
    const idCourse = req.body.idCourse;
    const nameLecture = req.body.nameLecture;
    const descriptionLecture = req.body.descriptionLecture;
    const fileKeep = req.body.fileKeep;
    try {
      const checkLecture = await LectureModel.findOne({ _id: idLecture });
      console.log(checkLecture);
      let dataFile = [];
      if (fileKeep) {
        for (let item of checkLecture.fileUpload) {
          console.log(item);
          const findItem = fileKeep.find((e) => e === item.filename);
          if (findItem) {
            dataFile.push(item);
          }
        }
      } else {
        dataFile = [...checkLecture.fileUpload];
      }
      if (req.files?.length > 0) {
        dataFile = [...dataFile, ...req.files];
      }
      if (checkLecture) {
        checkLecture.nameLecture = nameLecture || checkLecture.nameLecture;
        checkLecture.descriptionLecture = descriptionLecture || checkLecture.descriptionLecture;
        checkLecture.fileUpload = dataFile || checkLecture.fileUpload;
        checkLecture.save();
        return res.status(200).json({
          message: "Change lecture successfully",
          success: true,
          status: 200,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async getInformationLecture(req, res) {
    const idLecture = req.params.idLecture;

    try {
      const findLecture = await LectureModel.findOne({ _id: idLecture }).select("-__v");
      let data = [];
      if (findLecture) {
        for (let item of findLecture.fileUpload) {
          const object = {
            fieldname: item.fieldname,
            originalname: item.originalname,
            filename: item.filename,
            pathname: `http://api.itcode.vn:3000/${item.path.slice(19)}`,
            mimetype: item.mimetype,
            size: item.size,
          };
          data.push(object);
        }
      } else {
        return res.status(400).json({
          message: "Lecture not found",
          success: false,
          status: 400,
        });
      }

      const item = {
        nameLecture: findLecture.nameLecture,
        descriptionLecture: findLecture.descriptionLecture,
        idCourse: findLecture.idCourse,
        fileUpload: data,
        createDate: formatDatePost(findLecture.createDate),
      };

      return res.status(200).json({
        success: true,
        status_code: 200,
        data: item,
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
      });
    }
  }

  async getAllLecture(req, res) {
    const idCourse = req.query.idCourse;
    try {
      const findIdCourse = await LectureModel.find({ idCourse: idCourse });
      if (findIdCourse.length > 0) {
        var data = [];
        for (let item of findIdCourse) {
          const itemClone = {
            _id: item._id,
            nameLecture: item.nameLecture,
            descriptionLecture: item.descriptionLecture,
            idCourse: item.idCourse,
            createDate: formatDatePost(item.createDate),
          };
          data.push(itemClone);
        }
        return res.status(200).json({
          success: true,
          status_code: 200,
          data: data,
        });
      } else {
        return res.status(400).json({
          success: false,
          status_code: 400,
          message: "Lecture Not Found",
        });
      }
    } catch (err) {
      return res.status(500).json({
        success: false,
        status_code: 500,
      });
    }
  }
}

module.exports = new LectureController();
