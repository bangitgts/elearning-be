const ExerciseModel = require("../models/Exercise");
const AnswerModel = require("../models/Answer");

const CourseModel = require("../models/Course");
const ListDataModel = require("../models/ListData");
const saveFileModel = require("../models/SaveFile");
const Pagination = require("../utils/Pagination");
const { flagTime } = require("../utils/FlagTime");

var ObjectId = require("mongoose").Types.ObjectId;
const { v4: uuidv4 } = require("uuid");
const multipleUploadMiddleware = require("../middleware/multipleUploadMiddleware");
const { formatDatePost } = require("../utils/ParseTimeDay");
class ExerciseController {
  async addExerciseText(req, res) {
    const titleExercise = req.body.titleExercise;
    const idCourse = req.body.idCourse;
    const descriptionExercise = req.body.descriptionExercise;
    const isTextPoint = req.body.isTextPoint || 0; // 0 là điểm có số, 1 là không
    const allowSubmission = req.body.allowSubmission;
    const submissionDeadline = req.body.submissionDeadline;
    try {
      console.log(idCourse, req.user);
      const courseCheck = await CourseModel.findOne({ _id: idCourse, idTeacher: req.user._id });
      console.log(courseCheck);
      // const checkAccount = await AccountModel.findOne({ _id: req.user.id });
      //await multipleUploadMiddleware(req, res);
      if (isTextPoint == 0 || isTextPoint == 1) {
        const date = new Date();
        if (courseCheck) {
          ExerciseModel.create({
            idTeacher: req.user._id || null,
            idCourse: idCourse,
            nameCourse: courseCheck.nameCourse,
            titleExercise: titleExercise,
            isTextPoint: parseInt(isTextPoint),
            descriptionExercise: descriptionExercise,
            allowSubmission: allowSubmission || date,
            submissionDeadline: submissionDeadline || null,
            fileUpload: req.files,
            createDate: date,
          });
          return res.status(200).json({
            message: "Add exercise successfully",
            success: true,
            status: 200,
          });
        } else {
          return res.status(404).json({
            message: "Invalid course",
            success: false,
            status: 404,
          });
        }
      } else {
        return res.status(402).json({
          message: "Biến IsTextPoint không hợp lệ",
          success: false,
          status: 402,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: error,
        success: false,
        status: 500,
      });
    }
  }

  async getAllInformationExercise(req, res) {
    const { idExercise } = req.params;

    try {
      const findExercise = await ExerciseModel.findOne({ _id: idExercise }).select("-__v");
      const findAnswer = await AnswerModel.findOne({ idExercise: idExercise, idAccount: req.user._id });
      let data = [];
      const findTotalAnswer = await AnswerModel.find({ idExercise: idExercise });
      const findTotalAnswerScored = [];
      for (let item of findTotalAnswer) {
        if (item.studyPoint !== null) {
          findTotalAnswerScored.push(item);
        }
      }
      const findTotalStudent = await ListDataModel.count({ idCourse: findExercise.idCourse });
      if (findExercise) {
        for (let item of findExercise.fileUpload) {
          const object = {
            fieldname: item.fieldname,
            originalname: item.originalname,
            filename: item.filename,
            pathname: `http://api.itcode.vn:3000/${item.path.slice(19)}`,
            mimetype: item.mimetype,
            //pathname: item.pathname,
            size: item.size,
          };
          data.push(object);
        }
      }
      const item = {
        idAnswer: findAnswer !== null ? findAnswer._id : null,
        idExercise: findExercise._id,
        idTeacher: findExercise.idTeacher,
        idCourse: findExercise.idCourse,
        nameCourse: findExercise.nameCourse,
        titleExercise: findExercise.titleExercise,
        descriptionExercise: findExercise.descriptionExercise,
        isTextPoint: findExercise.isTextPoint,
        descriptionExercise: findExercise.descriptionExercise,
        allowSubmission: formatDatePost(findExercise.allowSubmission),
        submissionDeadline: findExercise.submissionDeadline ? formatDatePost(findExercise.submissionDeadline) : null,
        totalNumberOfSubmissions: findTotalAnswer.length,
        totalNumberOfGradedSubmissions: findTotalAnswerScored.length,
        totalStudentInCourse: findTotalStudent,
        files: data,
        createDate: formatDatePost(findExercise.createDate),
        updateDate: findExercise.updateDate ? formatDatePost(findExercise.updateDate) : null,
      };
      res.status(200).json({
        success: true,
        status_code: 200,
        data: item,
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
      });
    }
  } // get thong tin bai tap

  async getAllExerciseByIdCourse(req, res) {
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const idCourse = req.params.idCourse;
    var start = (page_number - 1) * page_size;
    var end = page_size * page_number;
    const getData = await ExerciseModel.find({
      idCourse: idCourse,
    }).select("-__v");
    const findAnswerOfCourse = await AnswerModel.find({ idCourse: idCourse, idAccount: req.user._id });

    let data = [];
    let totalNumberOfSubmissions = 0;
    for (let item of getData) {
      let checkItem = findAnswerOfCourse.find((e) => e.idExercise === item._id);
      if (checkItem) {
        totalNumberOfSubmissions = totalNumberOfSubmissions + 1;
      }
      const checkTimeSubission = flagTime(item.allowSubmission, item.submissionDeadline);

      const itemObject = {
        _id: item._id,
        idTeacher: item.idTeacher,
        idCourse: item.idCourse,
        nameCourse: item.nameCourse,
        titleExercise: item.titleExercise,
        isTextPoint: item.isTextPoint,
        fileUpload: item.fileUpload,
        descriptionExercise: item.descriptionExercise,
        isSubmitted: checkItem ? true : false,
        submittedTheTest: checkTimeSubission,
        allowSubmission: item.allowSubmission ? formatDatePost(item.allowSubmission) : null,
        submissionDeadline: item.submissionDeadline ? formatDatePost(item.submissionDeadline) : null,
        createDate: item.createDate ? formatDatePost(item.createDate) : null,
        updateDate: item.updateDate ? formatDatePost(item.updateDate) : null,
      };
      data.push(itemObject);
    }
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: data.length,
        page_size: page_size,
        page_number: page_number,
        totalNumberOfSubmissions: totalNumberOfSubmissions,
      },
      data: Pagination(data.reverse(), page_size, page_number),
    });
  } // by id course

  async getExerciseByIdAccount(req, res) {
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const idAccount = req.params.idAccount;
    var start = (page_number - 1) * page_size;
    var end = page_size * page_number;
    const getData = await ExerciseModel.find({
      idAccount: idAccount,
    }).select("-__v");
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getData.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: getData.slice(start, end),
    });
  } // by id course

  async getExerciseByIdExercise(req, res) {
    const getData = await ExerciseModel.find({
      _id: req.params.id,
    }).select("-__v");
    res.status(200).json({
      success: true,
      status_code: 200,
      data: getData,
    });
  } // by id course

  async editExerciseText(req, res) {
    try {
      const idExercise = req.body.idExercise;
      //const idExercise = req.params.idExercise;
      const titleExercise = req.body.titleExercise;
      const descriptionExercise = req.body.descriptionExercise;
      const allowSubmission = req.body.allowSubmission;
      const submissionDeadline = req.body.submissionDeadline;
      const checkExercise = await ExerciseModel.findOne({ _id: idExercise });
      const fileKeep = req.body.fileKeep;
      let dataFile = [];
      console.log("test0");
      console.log(fileKeep);
      console.log(checkExercise);
      if (fileKeep) {
        console.log("fileKepp test");
        for (let item of checkExercise.fileUpload) {
          console.log(item);
          const findItem = fileKeep.find((e) => e === item.filename);
          if (findItem) {
            dataFile.push(item);
          }
        }
      } else {
        dataFile = [...checkExercise.fileUpload];
      }
      console.log("test1");
      console.log(req.files);
      if (req.files?.length > 0) {
        dataFile = [...dataFile, ...req.files];
      }
      console.log("test2");
      console.log(submissionDeadline);
      console.log(typeof submissionDeadline);
      console.log(typeof null);

      if (checkExercise) {
        checkExercise.titleExercise = titleExercise || checkExercise.titleExercise;
        checkExercise.descriptionExercise = descriptionExercise || checkExercise.descriptionExercise;
        checkExercise.allowSubmission = allowSubmission || checkExercise.allowSubmission;
        checkExercise.submissionDeadline = submissionDeadline !== undefined ? (submissionDeadline === "null" ? null : submissionDeadline) : checkExercise.submissionDeadline;
        //checkExercise.submissionDeadline = null;
        checkExercise.fileUpload = dataFile || checkExercise.fileUpload;
        checkExercise.save();
        return res.status(200).json({
          message: "Change exercise successfully",
          success: true,
          status: 200,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async deleteExercise(req, res) {
    try {
      const idExercise = req.params.idExercise;
      const findExercise = await ExerciseModel.findOne({ _id: idExercise });
      const findAnswerOfExercise = await AnswerModel.find({ idExercise: idExercise });
      if (findExercise) {
        ExerciseModel.find({ _id: idExercise })
          .remove()
          .exec()
          .then((data) => {
            AnswerModel.find({ idExercise: idExercise }).remove().exec();
            return res.status(200).json({
              message: "Xóa thành công",
              success: true,
              status: 200,
            });
          });
      } else {
        return res.status(400).json({
          message: "Không tìm thấy bài tập này",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever Error",
        success: false,
        status: 500,
      });
    }
  }

  async getInformationExercise(req, res) {
    try {
      const idExercise = req.params.idExercise;
      const checkExercise = await ExerciseModel.findById(idExercise).select("-__v");
      if (checkExercise) {
        return res.status(200).json({
          success: true,
          status: 200,
          message: "Xuất dữ liệu thành công",
          data: checkExercise,
        });
      } else {
        return res.status(400).json({
          message: "ID không hợp lệ",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async getGradeExercise(req, res) {
    const idCourse = req.query.idCourse;
    const idExercise = req.query.idExercise;
    try {
      const listAllStudentCourse = await ListDataModel.find({ idCourse: idCourse });
      const listStudentAnswered = await AnswerModel.find({ idCourse: idCourse, idExercise: idExercise });
      const checkExercise = await ExerciseModel.findOne({ _id: idExercise });
      console.log(checkExercise);
      let data = [];
      for (let item of listAllStudentCourse) {
        const findItem = listStudentAnswered.find((e) => e.idAccount === item.idAccount);
        if (findItem) {
          const objectPush = {
            idAnswer: findItem._id,
            idAccount: item.idAccount,
            nameCourse: item.nameCourse,
            fullName: item.fullName,
            idExercise: parseInt(idExercise),
            idCourse: item.idCourse,
            nameCourse: item.nameCourse,
            studyPoint: findItem.studyPoint,
            isTextPoint: checkExercise.isTextPoint,
            createDate: findItem.createDate ? formatDatePost(findItem.createDate) : null,
            updateDate: findItem.updateDate ? formatDatePost(findItem.updateDate) : null,
          };
          data.push(objectPush);
        } else {
          const objectPush1 = {
            idAnswer: null,
            idAccount: item.idAccount,
            nameCourse: item.nameCourse,
            fullName: item.fullName,
            idCourse: item.idCourse,
            nameCourse: item.nameCourse,
            idExercise: parseInt(idExercise),
            studyPoint: null,
            isTextPoint: checkExercise.isTextPoint,
            createDate: null,
            updateDate: null,
          };
          data.push(objectPush1);
        }
      }
      return res.status(200).json({
        message: "Thanh Cong",
        success: true,
        status: 200,
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        message: "Thanh Cong",
        success: error,
      });
    }
  }
}
module.exports = new ExerciseController();
