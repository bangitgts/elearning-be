const LectureModel = require("../models/Lecture");
const CourseModel = require("../models/Course");
const AccountModel = require("../models/Account");
const ExerciseModel = require("../models/Exercise");
const AnswerModel = require("../models/Answer");
const ListDataModel = require("../models/ListData");

class ManagerController {
  async dashboardManager(req, res) {
    const totalLecture = await LectureModel.count({});
    const totalCourse = await CourseModel.count({});
    const totalExercise = await ExerciseModel.count({});
    const totalAnswer = await AnswerModel.count({});
    const totalStudent = await AccountModel.count({ role: "student" });
    const totalTeacher = await AccountModel.count({ role: "teacher" });
    res.status(200).json({
      status: 200,
      success: true,
      message: "Load successfully",
      data: {
        totalLecture,
        totalCourse,
        totalExercise,
        totalAnswer,
        totalStudent,
        totalTeacher,
      },
    });
  }
}

module.exports = new ManagerController();
