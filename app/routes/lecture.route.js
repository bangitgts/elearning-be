const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const checkAdmin = require("../auth/CheckAdmin");
const courseController = require("../controllers/CourseController");
const lectureController = require("../controllers/LectureController");
const multipleLectureUpload = require("../controllers/multipleUploadLectureController");

router.post("/OpenLecture", checkToken, multipleLectureUpload.multipleLectureUpload, lectureController.openLecture);
router.put("/EditLecture", checkToken, multipleLectureUpload.multipleLectureUpload, lectureController.editLecture);
router.delete("/DeleteLecture/:idLecture", checkToken, lectureController.deleteLecture);

router.get("/GetInformationLecture/:idLecture", checkToken, lectureController.getInformationLecture);
router.get("/GetAllLectureOfCourse", checkToken, lectureController.getAllLecture);

module.exports = router;
