const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const checkAdmin = require("../auth/CheckAdmin");
const checkStudent = require("../auth/CheckStudent");
const checkTeacher = require("../auth/CheckTeacher");
const classController = require("../controllers/ClassController");
const mutipleUploadController = require("../controllers/multipleUploadController");

router.post("/AddClass", classController.addClass);

router.delete("/DeleteClass/:idClass", checkToken, checkAdmin, classController.deleteClass);

router.post("/AddStudentToClass", classController.addStudentToClass);

router.get("/GetStudentByIdClass", classController.getStudentByIdClass);

router.get("/GetAllClass", classController.getAllClass);
module.exports = router;
