const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const managerController = require("../controllers/ManagerController");

router.get("/Dashboard", managerController.dashboardManager);

module.exports = router;
