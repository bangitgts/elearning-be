const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const checkStudent = require("../auth/CheckStudent");
const checkTeacher = require("../auth/CheckTeacher");
const answerController = require("../controllers/AnswerController");
const mutipleUploadController = require("../controllers/multipleUploadController");
const mutipleUploadGradingAnswer = require("../controllers/multipleUploadGradingAnswer");

router.post("/AddAnswer", checkToken, checkStudent, mutipleUploadController.multipleUpload, answerController.addAnswer);
router.put("/EditAnswer", checkToken, checkStudent, mutipleUploadController.multipleUpload, answerController.editAnswer);
router.delete("/DeleteAnswer/:idAnswer", checkToken, checkStudent, answerController.deleteAnswer);
//router.put("/ChangeAnswer", checkToken, checkStudent, answerController.changeAnswer);
router.get("/GetAnswerFromExercise", checkToken, answerController.getAnswerFromExercise);
router.get("/GetInformationAnswer", answerController.getInformationAnswer);
router.post("/GradingAssignment", checkToken, checkTeacher, mutipleUploadGradingAnswer.multipleUpload, answerController.gradingAssignment);
router.put("/EditGradingAssignment", checkToken, checkTeacher, mutipleUploadGradingAnswer.multipleUpload, answerController.editGradingAssignment);


module.exports = router;
