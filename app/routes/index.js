const accountRouter = require("./account.route");
const courseRouter = require("./course.route");
const exerciseRouter = require("./exercise.route");
const answerRouter = require("./answer.route");
const lectureRouter = require("./lecture.route");
const classRouter = require("./class.route");
const adminRouter = require("./admin.route");
function route(app) {
  app.use("/admin", adminRouter);
  app.use("/account", accountRouter);
  app.use("/course", courseRouter);
  app.use("/exercise", exerciseRouter);
  app.use("/answer", answerRouter);
  app.use("/lecture", lectureRouter);
  app.use("/class", classRouter);
}
module.exports = route;
